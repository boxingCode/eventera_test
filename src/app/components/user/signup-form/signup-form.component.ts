import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';


@Component({
  selector: 'signup-form',
  templateUrl: './signup-form.component.html',
  styleUrls: ['./signup-form.component.scss']
})
export class SignupFormComponent implements OnInit {
  inputFormControl;
  passwordLookup=false;emailLookup=false;usernameLookup:false;repeatpasswordLookup:false;

  constructor( private formBuilder:FormBuilder ) { 

    this.inputFormControl = formBuilder.group({
      username:'',
      email:'',
      password:'',
      passwordCheck:''
    });
  }

  ngOnInit() {
  }

  passwordCheck(password){
    console.log(password)
    /*if(password.length < 8){
      return false;
    }
    else{
      for(let i=0;i<password.length;i++){
      
      }
    }*/
    return true;
  }

  formSubmit(formDetails){
    console.log("FORM SUBMITTED");
  }

}
